const [ROWS, COLS] = [3, 3];
const [UP, RIGHT, DOWN, LEFT] = [1, 2, 3, 4];
let board = [];
let boardRef = [];
let direction = UP;

function createBoard() {
    let html = '<table>';
    for (let row = 0; row < ROWS; row++) {
      let boardRow = [];
      html += '<tr>';
      for (let col = 0; col < COLS; col++) {
        html += '<td id="c'+row+'_'+col+'"></td>';
        boardRow[col] = 0;
      }
      html += '</tr>';
      board.push(boardRow);
    }
    html += '</table>';
    document.getElementById('app').innerHTML = html;

    for (let row = 0; row < ROWS; row++) {
      let refRow = [];
      for (let col = 0; col < COLS; col++) {
        refRow[col] = document.getElementById('c'+row+'_'+col);
      }
      boardRef.push(refRow);
  }
}

function renderBoard() {
  for (let row = 0; row < ROWS; row++) {
    for (let col = 0; col < COLS; col++) {
      let cellClass = '';
      if (board[row][col] == 1) {
        cellClass = 'head';
      } else if (board[row][col] > 1) {
        cellClass = 'body';
      } else if (board[row][col] < 0) {
        cellClass = 'apple';
      }
      boardRef[row][col].className = cellClass;
    }
  }
}

function updateGame() {
  end:
  for (let row = 0; row < ROWS; row++) {
    for (let col = 0; col < COLS; col++) {
      if (board[row][col] > 0) {
        if (direction == UP) {
          let newRow = (row || ROWS) - 1;
          board[newRow][col] = 1;
          board[row][col] = 0;
          break end;
        }
      }
    }
  }
  renderBoard();
  setTimeout(updateGame, 500);
}

createBoard();

board[1][1] = 1; // head
//board[0][5] = -1; // apple

renderBoard();
updateGame()

